package arreglosactividad1;

import java.util.Scanner;

public class ArreglosActividad1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Problema1 p1 = new Problema1();

        Problema3 p3 = new Problema3();
        Problema4 p4 = new Problema4();
        Problema5 p5 = new Problema5();
        Problema6 p6 = new Problema6();
        Problema7 p7 = new Problema7();
        Problema8 p8 = new Problema8();
        Problema9 p9 = new Problema9();
        Problema10 p10 = new Problema10();

    }

    private static class Problema1 {

        public Problema1() {
            System.out.println("Problema 1");
            String[] alumnos = {"Rodrigo", "Carlos", "Josue", "Juan", "Orlando"};
            int[] edad = {19, 19, 20, 20, 20};
            int iPoint = 0;
            System.out.println(alumnos[iPoint]);
            System.out.println(edad[iPoint] + " años");
            System.out.println(alumnos[++iPoint]);
            System.out.println(edad[iPoint] + " años");
            System.out.println(alumnos[++iPoint]);
            System.out.println(edad[iPoint] + " años");
            System.out.println(alumnos[++iPoint]);
            System.out.println(edad[iPoint] + " años");
            System.out.println(alumnos[++iPoint]);
            System.out.println(edad[iPoint] + " años");
        }
    }

    private static class Problema3 {

        public Problema3() {
            System.out.println("Problema 3");
            int[] edad = {19, 19, 20, 20, 20};
            int suma = 0;
            int promedioE;
            int totalE = 0;

            for (int i : edad) {
                suma += i;
            }
            for (int i = 0; i < edad.length; i++) {
                totalE += edad[i];
            }
            promedioE = totalE / edad.length;
            System.out.println("La suma de las edades es: " + suma);
            System.out.println("El promedio de las edades es: " + promedioE + " años");

        }
    }

    private static class Problema4 {

        public Problema4() {
            System.out.println("problema 4");
            float[] numeros = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            float suma = 0;
            float promedio;

            for (float i : numeros) {
                suma += i;
            }

            promedio = suma / numeros.length;
            System.out.println("La suma de todos los numeros es: " + suma);
            System.out.println("El promedio es : " + promedio);

        }
    }

    private static class Problema5 {

        public Problema5() {
            Scanner sc = new Scanner(System.in);
            int numeroD;
            System.out.println("Problema 5");
            System.out.println("Introduzca un número del 1 al 7 para elegir un dia de la semana");
            numeroD = sc.nextInt();
            String[] día = {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"};

            if (numeroD < 8 && numeroD > 0) {
                System.out.println(día[numeroD - 1]);
            } else {
                System.out.println("Introduzca un numero valido");
            }

        }
    }

    private static class Problema6 {

        public Problema6() {
            System.out.println("Problema 6");

            String starWars[][] = new String[4][4];
            starWars[0][0] = "Luke Skywalker";
            starWars[0][1] = "R2-D2";
            starWars[0][2] = "C-3PO";
            starWars[0][3] = "Darth Vader";
            starWars[1][0] = "Leia Organa";
            starWars[1][1] = "Owen Lars";
            starWars[1][2] = "Beru Whitesun Lars";
            starWars[1][3] = "R5-D4";
            starWars[2][0] = "Biggs Darklighter";
            starWars[2][1] = "Obi-Wan Kenobi";
            starWars[2][2] = "Yoda";
            starWars[2][3] = "Jek Tono Porkins";
            starWars[3][0] = "Jabba Desilijic Tiure";
            starWars[3][1] = "Han Solo";
            starWars[3][2] = "Chewbacca";
            starWars[3][3] = "Anakin Skywalker";

            System.out.println("Personajes de Star Wars: ");
            for (int i = 0; i < 4; i++) {
                System.out.println(java.util.Arrays.toString(starWars[i]));
            }

            System.out.println("Personajes de Star Wars (for each): ");
            for (String[] i : starWars) {
                System.out.println(java.util.Arrays.toString(i));
            }

        }
    }

    private static class Problema7 {

        public Problema7() {
            Scanner sc = new Scanner(System.in);
            System.out.println("EJERCICIO 7");

            String[] codificacion = {"001-Calkini", "002-Campeche", "003-Carmen",
                "004-Champoton", "005-Hecelchakan", "006-Hopelchen",
                "007-Palizada", "008-Tenabo", "009-Escarcega",
                "010-Calakmul", "011-Candelaria", "012-Seybaplaya"};

            String[] cabeceras = {"Calkini", "San Francisco de Campeche", "Ciudad del Carmen",
                "Champoton", "Hecelchakan", "Hopelchen", "Palizada",
                "Tenabo", "Escarcega", "Xpujil", "Candelaria",
                "Seybaplaya"};

            int[] poblacionT = {52890, 259005, 221094, 83021, 28306, 37777,
                8352, 10665, 54184, 26882, 41194, 15420};

            for (int i = 0; i < 12; i++) {
                System.out.println(codificacion[i] + "   " + cabeceras[i] + "   " + poblacionT[i]);
            }

            int numero;

            System.out.println("Introduzca un numero del 1 al 12 para desplegar el municipio elegido");
            numero = sc.nextInt();

            if (numero < 13 && numero > 0) {
                System.out.println("El municipio elegido es: " + codificacion[numero - 1] + "---" + cabeceras[numero - 1] + "---" + poblacionT[numero - 1]);
            } else {
                System.out.println("Introduzca un numero valido");
            }

        }
    }

    private static class Problema8 {

        public Problema8() {
            Scanner sc = new Scanner(System.in);
            int num;

            System.out.println("problema 8");
            int[] prob8 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            System.out.println("Introduzca 1 si desea ver el primer dato del arreglo o escriba 2 si desea ver el ultimo");
            num = sc.nextInt();
            if (num == 1) {
                System.out.println("El primer dato del arreglo es: " + prob8[0]);
            }
            if (num == 2) {
                System.out.println("El ultimo dato del arreglos es: " + prob8[prob8.length - 1]);
            } else {
                System.out.println("Introduzca un numero valido");
            }

        }
    }

    public static class Problema9 {

        public Problema9() {
            Scanner sc = new Scanner(System.in);
            int numeroM;
            System.out.println("Problema 9");
            System.out.println("Introduzca un número del 1 al 12 para elegir un mes");
            numeroM = sc.nextInt();

            String[] mes = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

            if (numeroM < 13 && numeroM > 0) {
                System.out.println(mes[numeroM - 1]);
            } else {
                System.out.println("Inttroduzca un numero valido");
            }

        }
    }

    private static class Problema10 {

        public static void LeerArray(int[] arreglo) {
            String num7 = "";                          
            for (int i = 0; i < arreglo.length; i++) {               
                num7 = num7 + arreglo[i];
            }
            if (num7.indexOf("7") == -1) {               
                System.out.println("Aquí no se baila Mambo !!!");
            } else {
                System.out.println("Maaaambo");
            }
        }
        public Problema10() {
            System.out.println("problema10");
            int[] Arr1 = {1, 2, 3, 4, 5, 6, 7};                 
            int[] Arr2 = {8, 6, 33, 100};               
            int[] Arr3 = {2, 55, 37, 91, 65};            

            System.out.println("Arreglo 1");
            LeerArray(Arr1);                     

            System.out.println("Arreglo 2");
            LeerArray(Arr2);                     

            System.out.println("Arreglo 3");
            LeerArray(Arr3);

        }
    }

}
